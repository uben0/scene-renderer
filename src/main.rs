use array_matrix::MatricesExt;
use array_matrix::MatrixTrait;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::keyboard::Scancode;
use sdl2::pixels::Color;
use std::f64::consts::PI;
use std::time::Duration;

fn matrix_to_point([[x], [y]]: [[f64; 1]; 2]) -> sdl2::rect::Point {
    sdl2::rect::Point::new(x as i32, y as i32)
}

const X: [[f64; 1]; 3] = [[1.0], [0.0], [0.0]];
const Y: [[f64; 1]; 3] = [[0.0], [1.0], [0.0]];
const Z: [[f64; 1]; 3] = [[0.0], [0.0], [1.0]];

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("rust-sdl2 demo", 1000, 800)
        .resizable()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();

    let vertices = [
        [[0.0], [0.0], [0.0]],
        [[100.0], [0.0], [0.0]],
        [[0.0], [100.0], [0.0]],
        [[0.0], [0.0], [100.0]],
        [[100.0], [100.0], [100.0]],
        [[100.0], [100.0], [0.0]],
        [[0.0], [100.0], [100.0]],
        [[100.0], [0.0], [100.0]],
    ];
    let edges = [
        (0, 1, Color::RED),
        (0, 2, Color::GREEN),
        (0, 3, Color::BLUE),
        (4, 5, Color::BLACK),
        (4, 6, Color::BLACK),
        (4, 7, Color::BLACK),
        (1, 5, Color::BLACK),
        (1, 7, Color::BLACK),
        (2, 5, Color::BLACK),
        (2, 6, Color::BLACK),
        (3, 6, Color::BLACK),
        (3, 7, Color::BLACK),
    ];

    let (width, height) = canvas.window().size();
    let mut center_screen_origin = [[width as f64 / 2.0], [height as f64 / 2.0]];
    let screen_projection = [[-1.0, 0.0, 0.0], [0.0, -1.0, 0.0]];
    let mut pos = [[0.0], [0.0], [0.0]];

    fn h_rotate_c(h_angle: f64) -> [[f64; 3]; 3] {
        [
            [h_angle.cos(), 0.0, -h_angle.sin()],
            [0.0, 1.0, 0.0],
            [h_angle.sin(), 0.0, h_angle.cos()],
        ]
    }
    fn v_rotate_c(h_angle: f64) -> [[f64; 3]; 3] {
        [
            [1.0, 0.0, 0.0],
            [0.0, h_angle.sin(), h_angle.cos()],
            [0.0, -h_angle.cos(), h_angle.sin()],
        ]
    }
    fn h_norm_c(h_angle: f64) -> [[f64; 3]; 3] {
        [
            [h_angle.cos(), 0.0, h_angle.sin()],
            [0.0, 1.0, 0.0],
            [-h_angle.sin(), 0.0, h_angle.cos()],
        ]
    }
    let mut h_angle = 0.0;
    let mut v_angle = PI * 0.5;
    let mut h_rotate = h_rotate_c(h_angle);
    let mut v_rotate = v_rotate_c(v_angle);
    let mut h_norm = h_norm_c(h_angle);

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::MouseMotion { xrel, yrel, .. } => {
                    if xrel != 0 {
                        h_angle -= 0.01 * xrel as f64;
                        v_angle = v_angle.max(0.0).min(PI * 2.0);
                        h_rotate = h_rotate_c(h_angle);
                        h_norm = h_norm_c(h_angle);
                    }
                    if yrel != 0 {
                        v_angle -= 0.01 * yrel as f64;
                        v_angle = v_angle.max(0.0).min(PI);
                        v_rotate = v_rotate_c(v_angle);
                    }
                }
                Event::Window {
                    win_event: sdl2::event::WindowEvent::Resized(_, _),
                    ..
                } => {
                    let (width, height) = canvas.window().size();
                    center_screen_origin = [[width as f64 / 2.0], [height as f64 / 2.0]];
                }
                _ => {}
            }
        }

        let scale = |n| n * 3.0;
        let keyboard = event_pump.keyboard_state();
        if keyboard.is_scancode_pressed(Scancode::W) {
            pos += (h_norm.matrices() * Z).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::S) {
            pos -= (h_norm.matrices() * Z).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::A) {
            pos += (h_norm.matrices() * X).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::D) {
            pos -= (h_norm.matrices() * X).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::Space) {
            pos += (h_norm.matrices() * Y).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::LCtrl) {
            pos -= (h_norm.matrices() * Y).map(scale);
        }
        if keyboard.is_scancode_pressed(Scancode::Left) {
            h_angle += 0.02;
            h_angle = h_angle.min(PI * 2.0);
            h_rotate = h_rotate_c(h_angle);
            h_norm = h_norm_c(h_angle);
        }
        if keyboard.is_scancode_pressed(Scancode::Right) {
            h_angle -= 0.02;
            h_angle = h_angle.max(0.0);
            h_rotate = h_rotate_c(h_angle);
            h_norm = h_norm_c(h_angle);
        }
        if keyboard.is_scancode_pressed(Scancode::Up) {
            v_angle += 0.02;
            v_angle = v_angle.min(PI);
            v_rotate = v_rotate_c(v_angle);
        }
        if keyboard.is_scancode_pressed(Scancode::Down) {
            v_angle -= 0.02;
            v_angle = v_angle.max(0.0);
            v_rotate = v_rotate_c(v_angle);
        }
        canvas.set_draw_color(Color::WHITE);
        canvas.clear();
        let projected = vertices.map(|p| {
            let p = v_rotate * (h_rotate * (p - pos.matrices()));
            match p[2][0] * 0.003 {
                z if z > 0.0 => Some(
                    screen_projection
                        .matrix_mul(p)
                        .matrix_map(|n| n / z)
                        .matrix_add(center_screen_origin),
                ),
                _ => None,
            }
        });
        for (a, b, color) in edges {
            if let (Some(a), Some(b)) = (projected[a], projected[b]) {
                canvas.set_draw_color(color);
                canvas
                    .draw_line(matrix_to_point(a), matrix_to_point(b))
                    .unwrap();
            }
        }
        canvas.present();
        std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
